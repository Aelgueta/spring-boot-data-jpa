package com.bolsadeideas.springboot.app.controllers;
 import com.bolsadeideas.springboot.app.WebService.Municipalidad.*;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.entity.Arriendo;
import com.bolsadeideas.springboot.app.models.entity.Cliente;
import com.bolsadeideas.springboot.app.models.entity.ItemArriendo;
import com.bolsadeideas.springboot.app.models.entity.Producto;
import com.bolsadeideas.springboot.app.models.service.IClienteService;

@Controller
@RequestMapping("/arriendo")
@SessionAttributes("arriendo")
public class ArriendoController {

	@Autowired
	private IClienteService clienteService;
	
	private final Logger log = LoggerFactory.getLogger(getClass());
	
	@GetMapping(value= "/ver/{id}")
	public String ver(@PathVariable(value="id") Long id, 
			Model model,
			RedirectAttributes flash) {
		Arriendo arriendo = clienteService.findArriendoById(id);
		
		if(arriendo == null) {
			flash.addFlashAttribute("error", "La Reparacion no existe en la base de datos!");
			return "redirect:/listar";
		}
		
		model.addAttribute("arriendo", arriendo);
		model.addAttribute("titulo", "Realizar Arriendo");
		
		return "arriendo/ver";
	}

	@GetMapping("/form/{rut}")
	public String crear(@PathVariable(value = "rut") String rut, Map<String, Object> model,
			RedirectAttributes flash) {

		Cliente cliente = clienteService.findOne(rut);

		if (cliente == null) {
			flash.addFlashAttribute("error", "El cliente no existe en la base de datos");
			return "redirect:/listar";
		}

		Arriendo arriendo = new Arriendo();
		arriendo.setCliente(cliente);

		model.put("arriendo", arriendo);
		model.put("titulo", "Ingresar Arriendo");

		return "arriendo/form";
	}

	@GetMapping(value = "/cargar-productos/{term}", produces = { "application/json" })
	public @ResponseBody List<Producto> cargarProductos(@PathVariable String term) {
		return clienteService.findByNombre(term);
	}
	
	@PostMapping("/form/")
	public String guardar(Arriendo arriendo, 
			BindingResult result, Model model,
			@RequestParam(name = "item_id[]", required = false) Long[] itemId,
			@RequestParam(name = "cantidad[]", required = false) Integer[] cantidad,
			@RequestParam(name = "fecha_desde") @DateTimeFormat(pattern = "yyyy-MM-dd") Date desde,
			@RequestParam(name = "fecha_hasta") @DateTimeFormat(pattern = "yyyy-MM-dd") Date hasta,
			RedirectAttributes flash,
			SessionStatus status) {
		
		for (int i = 0; i < itemId.length; i++) {
			Producto producto = clienteService.findProductoById(itemId[i]);
			
			ItemArriendo linea = new ItemArriendo();
			linea.setProducto(producto);
			linea.setCantidad(cantidad[i]);
			arriendo.setFecha_desde(desde);
			arriendo.setFecha_hasta(hasta);
			arriendo.addItemArriendo(linea);

			log.info("ID: " + itemId[i].toString());
		}
		
		clienteService.saveArriendo(arriendo);
		status.setComplete();

		flash.addFlashAttribute("success", "Reparacion creada con éxito!");

		return "redirect:/ver/" + arriendo.getCliente().getRut();
	}
	
	@GetMapping(value ="/eliminar/{id}")
	public String eliminar(@PathVariable(value="id") Long id, RedirectAttributes flash) {
		
		Arriendo arriendo = clienteService.findArriendoById(id);
		
		if(arriendo != null) {
			clienteService.deleteArriendo(id);
			flash.addFlashAttribute("success", "Arriendo Eliminado con éxito!");
			return "redirect:/ver/" + arriendo.getCliente().getRut();
		}
		flash.addFlashAttribute("error", "El Arriendo no existe en la base de datos, no se pudo eliminar!");
		
		return "redirect:/listar";
	}

}
