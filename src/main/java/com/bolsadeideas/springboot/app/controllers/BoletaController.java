package com.bolsadeideas.springboot.app.controllers;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.entity.Cliente;
import com.bolsadeideas.springboot.app.models.entity.Boleta;
import com.bolsadeideas.springboot.app.models.entity.ItemBoleta;
import com.bolsadeideas.springboot.app.models.entity.Producto;
import com.bolsadeideas.springboot.app.models.service.IClienteService;

@Secured("ROLE_ADMIN")
@Controller
@RequestMapping("/boleta")
@SessionAttributes("boleta")
public class BoletaController {

	@Autowired
	private IClienteService clienteService;
	
	private final Logger log = LoggerFactory.getLogger(getClass());
	
	@GetMapping(value= "/ver/{id}")
	public String ver(@PathVariable(value="id") Long id, 
			Model model,
			RedirectAttributes flash) {
		Boleta boleta = clienteService.findBoletaById(id);
		
		if(boleta == null) {
			flash.addFlashAttribute("error", "La Boleta no existe en la base de datos!");
			return "redirect:/listar";
		}
		
		model.addAttribute("boleta", boleta);
		model.addAttribute("titulo", "Boleta: ".concat(boleta.getDescripcion()));
		
		return "boleta/ver";
	}

	@GetMapping("/form/{rut}")
	public String crear(@PathVariable(value = "rut") String rut, Map<String, Object> model,
			RedirectAttributes flash) {

		Cliente cliente = clienteService.findOne(rut);

		if (cliente == null) {
			flash.addFlashAttribute("error", "El cliente no existe en la base de datos");
			return "redirect:/listar";
		}

		Boleta boleta = new Boleta();
		boleta.setCliente(cliente);

		model.put("boleta", boleta);
		model.put("titulo", "Crear Boleta");

		return "boleta/form";
	}

	@GetMapping(value = "/cargar-productos/{term}", produces = { "application/json" })
	public @ResponseBody List<Producto> cargarProductos(@PathVariable String term) {
		return clienteService.findByNombre(term);
	}
	
	@PostMapping("/form")
	public String guardar(Boleta boleta, 
			BindingResult result, Model model,
			@RequestParam(name = "item_id[]", required = false) Long[] itemId,
			@RequestParam(name = "cantidad[]", required = false) Integer[] cantidad,
			RedirectAttributes flash,
			SessionStatus status) {
		
		for (int i = 0; i < itemId.length; i++) {
			Producto producto = clienteService.findProductoById(itemId[i]);
			
			ItemBoleta linea = new ItemBoleta();
			linea.setProducto(producto);
			linea.setCantidad(cantidad[i]);
			boleta.addItemBoleta(linea);

			log.info("ID: " + itemId[i].toString() + ", cantidad: " + cantidad[i].toString() );
		}
		
		clienteService.saveBoleta(boleta);
		status.setComplete();

		flash.addFlashAttribute("success", "Boleta creada con éxito!");

		return "redirect:/ver/" + boleta.getCliente().getRut();
	}
	
	@GetMapping(value ="/eliminar/{id}")
	public String eliminar(@PathVariable(value="id") Long id, RedirectAttributes flash) {
		
		Boleta boleta = clienteService.findBoletaById(id);
		
		if(boleta != null) {
			clienteService.deleteBoleta(id);
			flash.addFlashAttribute("success", "Boleta Eliminada con éxito!");
			return "redirect:/ver/" + boleta.getCliente().getRut();
		}
		flash.addFlashAttribute("error", "La Boleta no existe en la base de datos, no se pudo eliminar!");
		
		return "redirect:/listar";
	}

}
