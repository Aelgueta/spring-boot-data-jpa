package com.bolsadeideas.springboot.app.models.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.bolsadeideas.springboot.app.WebService.Municipalidad.MostrarClienteRutDescuentoResponse;
import com.bolsadeideas.springboot.app.WebService.Municipalidad.Municipalidad;

@Entity
@Table(name = "arriendos_items")
@SequenceGenerator(name="seq_items_arr", initialValue=1, allocationSize=1)
public class ItemArriendo implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="seq_items_arr")
	private Long id;
	
	private Integer cantidad;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="producto_id")
	private Producto producto;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Double calcularImporte() {
		
		Municipalidad muni = new Municipalidad();
		Arriendo arriendo = new Arriendo();
		String rut = arriendo.getCliente().getRut();
		muni.setRut(rut);
		MostrarClienteRutDescuentoResponse mostrarDesc = new MostrarClienteRutDescuentoResponse();
		mostrarDesc.setMunicipalidad(muni);
		
		Integer descuento = muni.getDescuento();
		
		return (cantidad.doubleValue() * producto.getPrecio_arriendo()) - descuento.doubleValue();
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}


	private static final long serialVersionUID = 1L;

}
