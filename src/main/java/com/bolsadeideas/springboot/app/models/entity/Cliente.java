package com.bolsadeideas.springboot.app.models.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "clientes")
public class Cliente {
	
	@Id
	@NotEmpty
	private String rut;

	@NotEmpty
	private String nombres;
	
	@NotEmpty
	private String apellido_paterno;
	
	@NotEmpty
	private String apellido_materno;
	
	@Column(length = 30, unique = true)
	private String username;

	@Column(length = 60)
	private String pass;

	private Boolean enabled;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "rut")
	private List<Role> roles;
	
	@NotEmpty
	@Email
	private String email;

	@NotNull
	@Column(name = "create_at")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date createAt;

	@OneToMany(mappedBy="cliente",fetch=FetchType.LAZY)
	private List<Boleta> boletas;
	
	@OneToMany(mappedBy="cliente",fetch=FetchType.LAZY)
	private List<Reparacion> reparaciones;
	
	@OneToMany(mappedBy="cliente",fetch=FetchType.LAZY)
	private List<Arriendo> arriendos;
	
	
	public List<Boleta> getBoletas() {
		return boletas;
	}
	
	public List<Reparacion> getReparaciones() {
		return reparaciones;
	}

	public void setBoletas(List<Boleta> boletas) {
		this.boletas = boletas;
	}
	
	public void setReparaciones(List<Reparacion> reparaciones) {
		this.reparaciones = reparaciones;
	}

	private void addBoleta(Boleta boleta) {
		boletas.add(boleta);
	}
	
	
	public Cliente() {
		boletas = new ArrayList<Boleta>();
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellido_paterno() {
		return apellido_paterno;
	}

	public void setApellido_paterno(String apellido_paterno) {
		this.apellido_paterno = apellido_paterno;
	}

	public String getApellido_materno() {
		return apellido_materno;
	}

	public void setApellido_materno(String apellido_materno) {
		this.apellido_materno = apellido_materno;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getCreateAt() {
		return createAt;
	}
	
	public List<Arriendo> getArriendos() {
		return arriendos;
	}

	public void setArriendos(List<Arriendo> arriendos) {
		this.arriendos = arriendos;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	
	
}
