package com.bolsadeideas.springboot.app.models.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "boletas",  uniqueConstraints= {@UniqueConstraint(columnNames={"id", "cliente_id"})})
@SequenceGenerator(name="seq_boleta", initialValue=1, allocationSize=1)
public class Boleta implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="seq_boleta")
	private Long id;
	private String descripcion;
	private Double monto_total;

	@Temporal(TemporalType.DATE)
	@Column(name = "created_at")
	private Date createAt;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cliente_id")
	private Cliente cliente;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL ,targetEntity = ItemBoleta.class)
	@JoinColumn(name = "boleta_id")
	private List<ItemBoleta> items;

	@PrePersist
	public void prePersist() {
		createAt = new Date();
	}
	
	public Boleta() {
		this.items = new ArrayList<ItemBoleta>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Double getMonto_total() {
		return monto_total;
	}

	public void setMonto_total(Double monto_total) {
		this.monto_total = monto_total;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<ItemBoleta> getItems() {
		return items;
	}

	public void setItems(List<ItemBoleta> items) {
		this.items = items;
	}
	
	public void addItemBoleta(ItemBoleta item) {
		this.items.add(item);
	}
	
	public Double getTotal() {
		Double total = 0.0; 
		
		int size = items.size();
		
		for(int i=0; i< size; i++) {
			total += items.get(i).calcularImporte();
		}
		return total;
				
	}
	

	private static final long serialVersionUID = 1L;

}
