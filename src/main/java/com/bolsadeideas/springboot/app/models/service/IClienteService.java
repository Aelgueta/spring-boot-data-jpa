package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.bolsadeideas.springboot.app.models.entity.Cliente;
import com.bolsadeideas.springboot.app.models.entity.Arriendo;
import com.bolsadeideas.springboot.app.models.entity.Boleta;
import com.bolsadeideas.springboot.app.models.entity.Producto;
import com.bolsadeideas.springboot.app.models.entity.Reparacion;

public interface IClienteService {

	public List<Cliente> findAll();

	public Page<Cliente> findAll(Pageable pageable);

	public void save(Cliente cliente);

	public Cliente findOne(String rut);

	public void delete(String rut);

	public List<Producto> findByNombre(String term);
	
	public Producto findProductoById(Long id);
	
	public void saveBoleta(Boleta boleta);
	
	public Boleta findBoletaById(Long id);
	
	public void deleteBoleta(Long id);
	
	public void saveReparacion(Reparacion reparacion);
	
	public Reparacion findReparacionById(Long id);
	
	public void deleteReparacion(Long id);
	
	public void saveArriendo(Arriendo arriendo);
	
	public Arriendo findArriendoById(Long id);
	
	public void deleteArriendo(Long id);

}
