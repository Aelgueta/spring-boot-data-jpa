package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bolsadeideas.springboot.app.models.dao.IArriendoDao;
import com.bolsadeideas.springboot.app.models.dao.IBoletaDao;
import com.bolsadeideas.springboot.app.models.dao.IClienteDao;
import com.bolsadeideas.springboot.app.models.dao.IProductoDao;
import com.bolsadeideas.springboot.app.models.dao.IReparacionDao;
import com.bolsadeideas.springboot.app.models.entity.Arriendo;
import com.bolsadeideas.springboot.app.models.entity.Boleta;
import com.bolsadeideas.springboot.app.models.entity.Cliente;
import com.bolsadeideas.springboot.app.models.entity.Producto;
import com.bolsadeideas.springboot.app.models.entity.Reparacion;

@Service
public class ClienteServiceImpl implements IClienteService {

	@Autowired
	private IClienteDao clienteDao;

	@Autowired
	private IProductoDao productoDao;
	
	@Autowired
	private IBoletaDao boletaDao;
	
	@Autowired
	private IReparacionDao reparacionDao;
	
	@Autowired
	private IArriendoDao arriendoDao;
	

	@Transactional(readOnly = true)
	public List<Cliente> findAll() {
		// TODO Auto-generated method stub
		return (List<Cliente>) clienteDao.findAll();
	}

	@Transactional
	public void save(Cliente cliente) {
		clienteDao.save(cliente);

	}

	@Transactional(readOnly = true)
	public Cliente findOne(String rut) {
		// TODO Auto-generated method stub
		return clienteDao.findById(rut).orElse(null);
	}

	@Transactional
	public void delete(String rut) {
		clienteDao.deleteById(rut);

	}

	@Transactional(readOnly = true)
	public Page<Cliente> findAll(Pageable pageable) {
		return clienteDao.findAll(pageable);
	}

	@Transactional(readOnly = true)
	public List<Producto> findByNombre(String term) {
		return productoDao.findByNombreLikeIgnoreCase("%" + term + "%");
	}
	
	@Transactional(readOnly=true)
	public Producto findProductoById(Long id) {
		// TODO Auto-generated method stub
		return productoDao.findById(id).orElse(null);
	}
	
	@Transactional
	public void saveReparacion(Reparacion reparacion) {
		reparacionDao.save(reparacion);
	}

	@Transactional(readOnly=true)
	public Reparacion findReparacionById(Long id) {
		// TODO Auto-generated method stub
		return reparacionDao.findById(id).orElse(null);
	}

	@Transactional
	public void deleteReparacion(Long id) {
		reparacionDao.deleteById(id);
	}
	
	@Transactional
	public void saveBoleta(Boleta boleta) {
		boletaDao.save(boleta);
	}

	@Transactional(readOnly=true)
	public Boleta findBoletaById(Long id) {
		// TODO Auto-generated method stub
		return boletaDao.findById(id).orElse(null);
	}

	@Transactional
	public void deleteBoleta(Long id) {
		boletaDao.deleteById(id);
	}

	public void saveArriendo(Arriendo arriendo) {
		// TODO Auto-generated method stub
		arriendoDao.save(arriendo);
		
	}

	public Arriendo findArriendoById(Long id) {
		// TODO Auto-generated method stub
		return arriendoDao.findById(id).orElse(null);
		
	}

	public void deleteArriendo(Long id) {
		// TODO Auto-generated method stub
		arriendoDao.deleteById(id);
		
	}

}
