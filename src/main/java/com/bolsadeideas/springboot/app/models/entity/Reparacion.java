package com.bolsadeideas.springboot.app.models.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "reparaciones")
@SequenceGenerator(name="seq_repa", initialValue=1, allocationSize=1)
public class Reparacion implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="seq_repa")
	private Long id;
	
	@Temporal(TemporalType.DATE)
	private Date fecha_estimada;
	
	private Double monto_total;

	@Temporal(TemporalType.DATE)
	@Column(name = "created_at")
	private Date createAt;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cliente_id")
	private Cliente cliente;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL ,targetEntity = ItemReparacion.class)
	@JoinColumn(name = "reparacion_id")
	private List<ItemReparacion> items;

	@PrePersist
	public void prePersist() {
		createAt = new Date();
	}
	
	public Reparacion() {
		this.items = new ArrayList<ItemReparacion>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getMonto_total() {
		return monto_total;
	}

	public void setMonto_total(Double monto_total) {
		this.monto_total = monto_total;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<ItemReparacion> getItems() {
		return items;
	}

	public void setItems(List<ItemReparacion> items) {
		this.items = items;
	}
	
	public void addItemReparacion(ItemReparacion item) {
		this.items.add(item);
	}
	
	public Date getFecha_estimada() {
		return fecha_estimada;
	}

	public void setFecha_estimada(Date fecha_estimada) {
		this.fecha_estimada = fecha_estimada;
	}

	private static final long serialVersionUID = 1L;

}
