package com.bolsadeideas.springboot.app.models.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="productos")
@SequenceGenerator(name="seq_prod", initialValue=1, allocationSize=1)
public class Producto implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="seq_prod")
	private Long id;
	
	private String nombre;
	
	private Long stock;
	
	private Double precio;
	private Double precio_arriendo;
	private Double precio_reparacion;
	
	@Temporal(TemporalType.DATE)
	@Column(name="create_at")
	private Date createAt;
	
	@PrePersist
	public void prePersist() {
		createAt = new Date();
	}
	
	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public Long getStock() {
		return stock;
	}



	public void setStock(Long stock) {
		this.stock = stock;
	}



	public Double getPrecio() {
		return precio;
	}



	public void setPrecio(Double precio) {
		this.precio = precio;
	}



	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public Double getPrecio_arriendo() {
		return precio_arriendo;
	}

	public void setPrecio_arriendo(Double precio_arriendo) {
		this.precio_arriendo = precio_arriendo;
	}

	public Double getPrecio_reparacion() {
		return precio_reparacion;
	}

	public void setPrecio_reparacion(Double precio_reparacion) {
		this.precio_reparacion = precio_reparacion;
	}



	private static final long serialVersionUID = 1L;

}
