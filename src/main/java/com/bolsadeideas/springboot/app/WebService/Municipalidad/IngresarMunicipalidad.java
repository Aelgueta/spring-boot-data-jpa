//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.07.06 a las 07:49:04 AM CLT 
//


package com.bolsadeideas.springboot.app.WebService.Municipalidad;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ingresarMunicipalidad complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ingresarMunicipalidad"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RUTCLIENTE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="VALOR_DESCUENTO" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="NOMBRE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="APELLIDOS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ingresarMunicipalidad", propOrder = {
    "rutcliente",
    "valordescuento",
    "nombre",
    "apellidos"
})
public class IngresarMunicipalidad {

    @XmlElement(name = "RUTCLIENTE")
    protected String rutcliente;
    @XmlElement(name = "VALOR_DESCUENTO")
    protected int valordescuento;
    @XmlElement(name = "NOMBRE")
    protected String nombre;
    @XmlElement(name = "APELLIDOS")
    protected String apellidos;

    /**
     * Obtiene el valor de la propiedad rutcliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRUTCLIENTE() {
        return rutcliente;
    }

    /**
     * Define el valor de la propiedad rutcliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRUTCLIENTE(String value) {
        this.rutcliente = value;
    }

    /**
     * Obtiene el valor de la propiedad valordescuento.
     * 
     */
    public int getVALORDESCUENTO() {
        return valordescuento;
    }

    /**
     * Define el valor de la propiedad valordescuento.
     * 
     */
    public void setVALORDESCUENTO(int value) {
        this.valordescuento = value;
    }

    /**
     * Obtiene el valor de la propiedad nombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNOMBRE() {
        return nombre;
    }

    /**
     * Define el valor de la propiedad nombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNOMBRE(String value) {
        this.nombre = value;
    }

    /**
     * Obtiene el valor de la propiedad apellidos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAPELLIDOS() {
        return apellidos;
    }

    /**
     * Define el valor de la propiedad apellidos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAPELLIDOS(String value) {
        this.apellidos = value;
    }

}
