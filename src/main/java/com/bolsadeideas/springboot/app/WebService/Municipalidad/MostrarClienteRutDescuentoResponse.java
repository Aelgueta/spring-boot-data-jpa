//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.07.06 a las 07:49:04 AM CLT 
//


package com.bolsadeideas.springboot.app.WebService.Municipalidad;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para MostrarClienteRutDescuentoResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="MostrarClienteRutDescuentoResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Municipalidad" type="{http://WebService/}municipalidad" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MostrarClienteRutDescuentoResponse", propOrder = {
    "municipalidad"
})
public class MostrarClienteRutDescuentoResponse {

    @XmlElement(name = "Municipalidad")
    protected Municipalidad municipalidad;

    /**
     * Obtiene el valor de la propiedad municipalidad.
     * 
     * @return
     *     possible object is
     *     {@link Municipalidad }
     *     
     */
    public Municipalidad getMunicipalidad() {
        return municipalidad;
    }

    /**
     * Define el valor de la propiedad municipalidad.
     * 
     * @param value
     *     allowed object is
     *     {@link Municipalidad }
     *     
     */
    public void setMunicipalidad(Municipalidad value) {
        this.municipalidad = value;
    }

}
