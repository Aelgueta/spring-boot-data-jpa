//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.07.06 a las 07:49:04 AM CLT 
//


package com.bolsadeideas.springboot.app.WebService.Municipalidad;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.bolsadeideas.springboot.app.WebService.Municipalidad package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MostrarClienteRutDescuento_QNAME = new QName("http://WebService/", "MostrarClienteRutDescuento");
    private final static QName _MostrarClienteRutDescuentoResponse_QNAME = new QName("http://WebService/", "MostrarClienteRutDescuentoResponse");
    private final static QName _IngresarMunicipalidad_QNAME = new QName("http://WebService/", "ingresarMunicipalidad");
    private final static QName _IngresarMunicipalidadResponse_QNAME = new QName("http://WebService/", "ingresarMunicipalidadResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.bolsadeideas.springboot.app.WebService.Municipalidad
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MostrarClienteRutDescuento }
     * 
     */
    public MostrarClienteRutDescuento createMostrarClienteRutDescuento() {
        return new MostrarClienteRutDescuento();
    }

    /**
     * Create an instance of {@link MostrarClienteRutDescuentoResponse }
     * 
     */
    public MostrarClienteRutDescuentoResponse createMostrarClienteRutDescuentoResponse() {
        return new MostrarClienteRutDescuentoResponse();
    }

    /**
     * Create an instance of {@link IngresarMunicipalidad }
     * 
     */
    public IngresarMunicipalidad createIngresarMunicipalidad() {
        return new IngresarMunicipalidad();
    }

    /**
     * Create an instance of {@link IngresarMunicipalidadResponse }
     * 
     */
    public IngresarMunicipalidadResponse createIngresarMunicipalidadResponse() {
        return new IngresarMunicipalidadResponse();
    }

    /**
     * Create an instance of {@link Municipalidad }
     * 
     */
    public Municipalidad createMunicipalidad() {
        return new Municipalidad();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MostrarClienteRutDescuento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebService/", name = "MostrarClienteRutDescuento")
    public JAXBElement<MostrarClienteRutDescuento> createMostrarClienteRutDescuento(MostrarClienteRutDescuento value) {
        return new JAXBElement<MostrarClienteRutDescuento>(_MostrarClienteRutDescuento_QNAME, MostrarClienteRutDescuento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MostrarClienteRutDescuentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebService/", name = "MostrarClienteRutDescuentoResponse")
    public JAXBElement<MostrarClienteRutDescuentoResponse> createMostrarClienteRutDescuentoResponse(MostrarClienteRutDescuentoResponse value) {
        return new JAXBElement<MostrarClienteRutDescuentoResponse>(_MostrarClienteRutDescuentoResponse_QNAME, MostrarClienteRutDescuentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IngresarMunicipalidad }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebService/", name = "ingresarMunicipalidad")
    public JAXBElement<IngresarMunicipalidad> createIngresarMunicipalidad(IngresarMunicipalidad value) {
        return new JAXBElement<IngresarMunicipalidad>(_IngresarMunicipalidad_QNAME, IngresarMunicipalidad.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IngresarMunicipalidadResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebService/", name = "ingresarMunicipalidadResponse")
    public JAXBElement<IngresarMunicipalidadResponse> createIngresarMunicipalidadResponse(IngresarMunicipalidadResponse value) {
        return new JAXBElement<IngresarMunicipalidadResponse>(_IngresarMunicipalidadResponse_QNAME, IngresarMunicipalidadResponse.class, null, value);
    }

}
