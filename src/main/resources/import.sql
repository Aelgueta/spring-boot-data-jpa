/* Populate tables */
insert into clientes (rut, nombres, apellido_paterno, apellido_materno, email, create_at,username,pass,enabled) values('19340717-K', 'Ashim Joshua', 'Elgueta', 'Salinas', 'aelgueta@tricot.cl', SYSDATE,'admin','$2a$10$O9wxmH/AeyZZzIS09Wp8YOEMvFnbRVJ8B4dmAMVSGloR62lj.yqXG',1);
insert into clientes (rut, nombres, apellido_paterno, apellido_materno, email, create_at,username,pass,enabled) values('18676371-8', 'Doris Scarlet', 'Paredes', 'Painecura', 'dparedes@tricot.cl', SYSDATE,'doris','$2a$10$DOMDxjYyfZ/e7RcBfUpzqeaCs8pLgcizuiQWXPkU35nOhZlFcE9MS',1);


insert into productos (id,nombre, stock,precio, precio_arriendo, precio_reparacion,create_at) values(seq_prod.NEXTVAL,'Giant Talon29 4GI/2019', 4,240000,50000,60000, SYSDATE);
insert into productos (id,nombre, stock,precio, precio_arriendo, precio_reparacion,create_at) values(seq_prod.NEXTVAL,'Scott Cannondale', 9,359000,70000,80000, SYSDATE);

INSERT INTO authorities (id, rut, authority) VALUES (seq_role.NEXTVAL,'18676371-8','ROLE_USER');
INSERT INTO authorities (id, rut, authority) VALUES (seq_role.NEXTVAL,'19340717-K','ROLE_ADMIN');
INSERT INTO authorities (id, rut, authority) VALUES (seq_role.NEXTVAL,'18676371-8','ROLE_USER');
